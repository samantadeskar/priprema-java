package hr.siemenscvc.praksa.samantadeskar.zadatak14;


public class Foursquare {
	private double diagonal;
	private double a;
	
	public double getDiagonal() {
		return diagonal;
	}
	public void setDiagonal(double diagonal) {
		this.diagonal = diagonal;
	}
	public double getA() {
		return a;
	}
	public void setA(double a) {
		this.a = a;
	}
	
}
