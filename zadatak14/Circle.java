package hr.siemenscvc.praksa.samantadeskar.zadatak14;

public class Circle {
	
	private double r;

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}
	
	public void calculateO(double r){
		double o = 2*r*3.14;
		System.out.println("Opseg kruga je " + o + " cm.");
	}
	
	public void calculateP(double a){
		double p = (r*r*3.14)/4;
		System.out.println("Povrsina kruga je " + p + " cm^2.");
	}
}
