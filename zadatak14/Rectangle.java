package hr.siemenscvc.praksa.samantadeskar.zadatak14;


public class Rectangle extends Foursquare{
	private double b;
	
	
	public void diagonalCalculation(double a, double b){
		double result;
		result = Math.sqrt(a*a + b*b);
		super.setDiagonal(result); 
	}


	public double getB() {
		return b;
	}


	public void setB(double b) {
		this.b = b;
	}
	
	public void calculateO(double a, double b){
		double o = 2*(a+b);
		System.out.println("Opseg pravokutnika je " + o + " cm.");
	}
	
	public void calculateP(double a, double b){
		double p = (a*b);
		System.out.println("Povrsina pravokutnika je " + p + " cm^2.");
	}
	
}
