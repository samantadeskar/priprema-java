package hr.siemenscvc.praksa.samantadeskar.zadatak14;

public class Triangle {
	
	private double a;
	private double b;
	private double c;
	
	public double getA() {
		return a;
	}
	public void setA(double a) {
		this.a = a;
	}
	public double getB() {
		return b;
	}
	public void setB(double b) {
		this.b = b;
	}
	public double getC() {
		return c;
	}
	public void setC(double c) {
		this.c = c;
	}
	
	public void calculateO(double a, double b, double c){
		double o = a+b+c;
		System.out.println("Opseg trokuta je " + o + " cm.");
	}
	
	public void calculateP(double a, double b, double c){
		double s = (a+b+c)/2;
		double p = Math.sqrt(s*(s-a)*(s-b)*(s-c));
		System.out.println("Povrsina trokuta je " + p + " cm^2.");
	}
	
}
