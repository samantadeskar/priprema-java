package hr.siemenscvc.praksa.samantadeskar.zadatak14;

import java.util.Scanner;


public class Main {

	
	
	public static void main(String[] args) {
		Scanner keyboard = new Scanner (System.in);
		
		int a;
		do{	
		System.out.println("Odaberite geometrijski lik: " + 
							"\n1 - Pravokutnik" + 
							"\n2 - Kvadrat" + 
							"\n3 - Trokut" + 
							"\n4 - Krug");
		
		a = keyboard.nextInt();
		if (a == 0){System.out.println("Izasli ste iz programa."); break;}
		switch (a){
			
		case 1:
			System.out.println("Unesite stranicu a:");
			double x = keyboard.nextInt();
			System.out.println("Unesite stranicu b:");
			double b = keyboard.nextInt();
			Rectangle pravokutnik = new Rectangle();
			pravokutnik.setA(x);
			pravokutnik.setB(b);
			pravokutnik.calculateO(x, b);
			pravokutnik.calculateP(x, b);
			pravokutnik.diagonalCalculation(x, b);
			System.out.println("Dijagonala pravokutnika je " + pravokutnik.getDiagonal());
			
			
			break;
			
		case 2:
			System.out.println("Unesite stranicu kvadrata:");
			double y = keyboard.nextInt();
			Square kvadrat = new Square();
			kvadrat.setA(y);
			kvadrat.calculateO(y);
			kvadrat.calculateP(y);
			kvadrat.diagonalCalculation(y);
			System.out.println("Dijagonala kvadrata je " + kvadrat.getDiagonal());
			break;
			
		case 3:
			System.out.println("Unesite stranicu a:");
			double z = keyboard.nextInt();
			System.out.println("Unesite stranicu b:");
			double i = keyboard.nextInt();
			System.out.println("Unesite stranicu c:");
			double j = keyboard.nextInt();
			Triangle trokut = new Triangle();
			trokut.setA(z);
			trokut.setB(i);
			trokut.setC(j);
			trokut.calculateO(z, i, j);
			trokut.calculateP(z, i, j);
			break;
			
		case 4:
			System.out.println("Unesite radijus:");
			double r = keyboard.nextInt();
			Circle krug = new Circle();
			krug.calculateO(r);
			krug.calculateP(r);
			break;
			
		default:
			System.out.println("Krivi odabir, pokusajte ponovno.");	
		}
	}while (a != 0);
  
	keyboard.close();
	}
}
