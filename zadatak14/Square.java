package hr.siemenscvc.praksa.samantadeskar.zadatak14;


public class Square extends Foursquare {

	public void diagonalCalculation(double a){
		double result = Math.sqrt(a*a);
		super.setDiagonal(result);
	}
	
	public void calculateO(double a){
		double o = 4*a;
		System.out.println("Opseg kvadrata je " + o + " cm.");
	}
	
	public void calculateP(double a){
		double p = a*a;
		System.out.println("Povrsina kvadrata je " + p + " cm^2.");
	}
}
