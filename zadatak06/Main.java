package hr.siemenscvc.praksa.samantadeskar.zadatak06;
import java.util.Scanner;

public class Main {

	private static Scanner keyboard = new Scanner (System.in);
	
	public static void main(String[] args) {
		
		System.out.println("Unesite ime: ");
		String ime = keyboard.next();
		System.out.println("-------------------------------");
		System.out.println(": : :  TABLICA MNO�ENJA  : : :");
		System.out.println("-------------------------------");
		System.out.print("* |  ");
		
		
		for (int i = 1; i<=9; i++){
			System.out.print( i + "  ");
		}
		System.out.println("\n-------------------------------");
		for (int i = 1; i<=9; i++){
			System.out.print(i + " |");
			for (int j=1; j<=9; j++){
				if (i*j<10) System.out.print("  " + i*j);
				else System.out.print(" " + i*j);
			}
			System.out.println();
		}
		
		
		System.out.println("-------------------------------");
		String s = ":  :  :  :  :  :  :  :  :  :  :";
		System.out.println(s.substring(0,(31-ime.length()-3)) + "by " + ime);

	}

}
