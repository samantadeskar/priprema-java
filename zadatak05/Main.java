package hr.siemenscvc.praksa.samantadeskar.zadatak05;

/*05)	Kreiraj program koji ima definiran po�etak i kraj intervala te jedan broja�. 
  Neka po�etak intervala bude manji od 10, a kraj intervala ve�i od 100. 
  Program treba provjeriti jesu li po�etak i kraj intervala ispravno definirani te ispisati 
  poruku o pogre�ci ukoliko nisu. Ukoliko jesu program treba za svaki broj u zadanom intervalu 
  napraviti sljede�e:
o	ukoliko je broj manji ili jednak 15, broja� treba pove�ati za 5
o	ukoliko je broj ve�i od 15, broja� treba umanjiti za 1
o	ukoliko je broj djeljiv s 20, treba prekinuti procesuiranje tog broja i prije�i na sljede�i
o	ukoliko je broj jednak 75, treba prekinuti procesuiranje intervala te ispisati 
	vrijednost broja�a
*/

public class Main {

	public static void main(String[] args) {

		int a = 9;
		int b = 101;
		int sum = 0;
		
		if (a >= 10 && a <= 100){
			System.out.println ("Pocetak i kraj intervala nisu ispravni.");
		}
		
		else{
			for (int i = a; i <= b; i++){
				if (i < 15)
				{
					sum += 5;
				}
				else if (i > 15)
				{
					sum--;
					if (i % 20 == 0){
						i++;
					}
					else if (i == 75){
						i = b+1;
						System.out.println ("Vrijednost brojaca je " + sum);
					}
				}
			}
		}
	}
}

