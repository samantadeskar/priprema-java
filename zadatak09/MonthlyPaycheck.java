package hr.siemenscvc.praksa.samantadeskar.zadatak09;

import java.util.Scanner;

public class MonthlyPaycheck {
	
	public int insertPaycheck(){
		Scanner keyboard = new Scanner (System.in);
		int paycheck = keyboard.nextInt();
		if (paycheck<=500) paycheck = 0;
		else if (paycheck>500 && paycheck<=1500) paycheck =1000;
		else if (paycheck>1500 && paycheck<=2500) paycheck =2000;
		else if (paycheck>2500 && paycheck<=3500) paycheck =3000;
		else if (paycheck>3500 && paycheck<=4500) paycheck =4000;
		else if (paycheck>4500) paycheck =5000;
		keyboard.close();
		return paycheck;
	}
	
}
