package hr.siemenscvc.praksa.samantadeskar.zadatak08;
import java.util.Scanner;

public class Main {

	private static Scanner keyboard = new Scanner (System.in);
	
	public static void main(String[] args) {
		System.out.println ("Unesite broj mjeseca: ");
		int[] mjesec = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		int a = keyboard.nextInt();
		
		if (a>=1 && a <= 12){
			System.out.println(" P  U  S  �  P  S  N");
			int counter = 0;
			for (int i = 1; i<=mjesec[a-1]; i++){
				counter++;
				if(counter == 1){
					if(i <10)System.out.print(" "+i+"  ");
					else System.out.print(i+" ");
					
				}else if(counter == 7){
					counter = 0;
					System.out.print(i);
					System.out.println();
				}
				else{
					if(i <10 && i+1<10)System.out.print(i+"  ");
					else if(i<10 && i+1>10)System.out.print(i+" ");
					else System.out.print(i+" ");
				}
			
			}
		}
		else System.out.println("Uneseni mjesec ne postoji.");
	}

}
