package hr.siemenscvc.praksa.samantadeskar.zadatak13;

public class Main {

	public static void main(String[] args) {
		Dog dog = new Dog ();
		dog.setName("Doggy");
		dog.setAge(3);
		
		Bird bird = new Bird();
		bird.setName("Tweety");
		bird.setAge(1);
		
		dog.catchBall();
		dog.move();
		dog.speak();
		
		bird.catchBall();
		bird.move();
		bird.speak();
	}

}
