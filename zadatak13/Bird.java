package hr.siemenscvc.praksa.samantadeskar.zadatak13;

public class Bird implements MyInterface {
	String name;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	int age;
	void speak(){System.out.println("Tweet, tweet");}
	
	@Override
	public void move() {
		System.out.println(this.getName() + " is now flying");
		
	}
	@Override
	public void catchBall() {
		System.out.println(this.getName() + " can't catch the ball because it's bird");
	}
	
	@Override
	public String toString() {
		return name;
	}
}
