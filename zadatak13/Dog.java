package hr.siemenscvc.praksa.samantadeskar.zadatak13;

public class Dog implements MyInterface {
	private String name;
	private int age;
	void speak(){System.out.println("Bark, bark");}
	
	@Override
	public void move() {
		System.out.println(this.getName() + " is now running");
		
	}
	@Override
	public void catchBall() {
		System.out.println(this.getName() + " catched the ball");
	}
	
	@Override
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
