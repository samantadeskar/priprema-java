package hr.siemenscvc.praksa.samantadeskar.zadatak04;

public class IntArray {

	public static void main(String[] args) {
		 int[] integerArray = {2,65,34,91,86};
		 checkIntegers (integerArray);
	 }

	 public static void checkIntegers (int[] userArray){
		 for (int i = 0; i < userArray.length; i++){
			 if ((userArray[i] % 2) == 0){
				 System.out.println (userArray[i] + " je paran broj.");
			 }
			 else{
				 System.out.println (userArray[i] + " je neparan broj.");

			 }
		 }
	 }
}
