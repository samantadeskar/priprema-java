package hr.siemenscvc.praksa.samantadeskar.zadatak07;

import java.util.Scanner;
public class Main {

	private static Scanner keyboard = new Scanner (System.in);
	
	public static void main(String[] args) {

		System.out.println ("Unesite pocetni broj intervala: ");
		int a = keyboard.nextInt();
		System.out.println("\nUnesite zadnji broj intervala: ");
		int b = keyboard.nextInt();
		
		int sum = 0;
		
		for (int i = a; i<=b; i++){
			if (i % 7 == 0){
				sum++;
			}
		}
		
		System.out.println ("Broj brojeva djeljivih sa 7 je " + sum);

	}

}
