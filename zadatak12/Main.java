package hr.siemenscvc.praksa.samantadeskar.zadatak12;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.io.File;
import java.io.FileReader;
import java.lang.String;

public class Main {

	public static String unosDatoteke() {

		Scanner reader = new Scanner(System.in);
		System.out.println("Unesite putanju datoteke za citanje!");
		String putanja = reader.nextLine();
		reader.close();
		return putanja;

	}

	public static Comparator<String> ALPHABETICAL_ORDER = new Comparator<String>() {
		public int compare(String str1, String str2) {
			int res = String.CASE_INSENSITIVE_ORDER.compare(str1, str2);
			if (res == 0) {
				res = str1.compareTo(str2);
			}
			return res;
		}
	};

	public static ArrayList<String> upisRijeci() {
		ArrayList<String> listaRijeci = new ArrayList<>();
		Scanner reader = new Scanner(System.in);
		String rijec = "";
		while (!rijec.equals("kraj")) {
			System.out
					.println("Upisite rijec koji zelite prebrojati u fileu (za kraj upisi: kraj ");
			rijec = reader.next();
			if (!rijec.toLowerCase().trim().equals("kraj"))
				listaRijeci.add(rijec.toLowerCase().trim());
		}
		reader.close();
		return listaRijeci;
	}

	public static void ispisRijeci(ArrayList<String> lista) {
		System.out.println("VASA LISTA RIJECI IZGLEDA: ");
		lista.forEach((_item) -> {
			System.out.println(_item);
		});
	}

	public static void main(String[] args) {
		ArrayList<Integer> brojaci = new ArrayList<>();

		ArrayList<String> listaRijeci = new ArrayList<>();
		listaRijeci = upisRijeci();
		for (int i = 0; i < listaRijeci.size(); i++) {
			brojaci.add(0);
		}
		Collections.sort(listaRijeci, ALPHABETICAL_ORDER);
		ispisRijeci(listaRijeci);
		File file = new File(unosDatoteke());

		// sve radi osim try-catch dijela
		try {
			Scanner input = new Scanner(new FileReader(file));
			input.useDelimiter(" +");
			while (input.hasNext()) {
				for (int i = 0; i < listaRijeci.size(); i++) {
					if (input.next().equals(listaRijeci.get(i))) {
						int adder = Integer.valueOf(brojaci.get(i).toString());
						adder++;
						brojaci.set(i, adder);
					}
				}
				input.close();

			}
		} catch (Exception e) {
			System.out.println("nije otvorilo");
		}
		for (int i = 0; i < listaRijeci.size(); i++) {
			System.out.println(listaRijeci.get(i) + " : " + brojaci.get(i));

		}

	}
}
